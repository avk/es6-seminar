/* jslint esnext:true, node:true*/
/*global console,window,XMLHttpRequest,Promise,document */

"use strict"; // required for using "let" in chrome browser.

var makeRequest = (url) => {
  return new Promise((resolve, reject) => {
    let httpRequest = new XMLHttpRequest(); // Only works in Gecko or WebKit

    httpRequest.onreadystatechange = () => {
      if (httpRequest.readyState === 4) { // 4 is complete state
        if (httpRequest.status === 200) { // response OK
          resolve(httpRequest.responseText);
        } else {
          reject();
        }
      } else {
        // still not ready
      }
    };

    httpRequest.open('GET', url, true);
    httpRequest.send(null);

  }); // promise object
};

window.onload = () => {
  let calendarURL = 'cal.ics',
      keywords = ['VCALENDAR', 'VTIMEZONE', 'DAYLIGHT', 'STANDARD', 'VEVENT', 'VTODO', 'VALARM'],
      iCalData = {
          name : "iCalMain",
          values : {},
          __parent : {}
        },
      state=iCalData,
      events = [],
      currentEvent = null,
      eventsThisWeek = new Array(7);

  makeRequest(calendarURL).then((data) => {
    // https://www.ietf.org/rfc/rfc2445.txt (iCal RFC)
    let lines;

    data = data.replace(/(\r\n )|(\n )/,''); // long lines continuation start with a space
    lines = data.split("\r\n"); // each line delimited with a CRLF sequence

    for(let line of lines){
      // ignore empty lines
      if(!line) continue;

      let [key, value] = line.split(':');

      // if it is a keyword
      if(keywords.indexOf(value) !== -1){

        if(key === "BEGIN"){
          if(state.values[value] === undefined)
            state.values[value] = [];

          let index = state.values[value].push({ name: value, values:{}, __parent: state, attrs: {} });
          state = state.values[value][index-1];
        } else if(key === "END"){
          state = state.__parent;
        }

      // if not a keyword then add it to the current state variable
      } else {
        state.attrs[key] = value;
      }

    }

    for(let line of lines){
      if(!line) continue;
      let today = new Date(), endOfWeek = new Date(),
          [key, value] = line.split(':'),
          key_attrs = null,
          event_container = document.getElementById('events');
      today.setHours(0,0,0,0); // get todays Date in midnight
      endOfWeek.setDate(today.getDate() + 7);

      if(key.indexOf(';') !== -1){
        [key, key_attrs] = key.split(";");
      }
      // finish the currentEvent state if its the end of the event
      if(key === "END" && value === "VEVENT"){
        currentEvent.SUMMARY = currentEvent.SUMMARY || '';
        currentEvent.LOCATION = currentEvent.LOCATION || '';
        currentEvent.DESCRIPTION = currentEvent.DESCRIPTION || '';
        var event_details = `<div class="event">
          <div class="day">
            ${currentEvent.DTSTART.getDay()} 
            ${currentEvent.DTSTART.toLocaleString('en-US', { month: "short" })} 
            ${currentEvent.DTSTART.getFullYear()}
          </div>
          <div class="summary">${currentEvent.SUMMARY}</div>
          <div class="description">${currentEvent.DESCRIPTION}</div>
          <div class="location">${currentEvent.LOCATION}</div>
          <div class="clearfix"></div>
        </div>`;
        event_container.insertAdjacentHTML('beforeend',event_details);
        currentEvent = null;
      }

      // if we are inside an event then add the attributes
      if(currentEvent !== null){
        if(key === "DTSTART" || key === "DTEND")
        {
          let temp = value;
          value = new Date(temp[0]+temp[1]+temp[2]+temp[3],
                           parseInt(temp[4]+temp[5]) - 1,
                           temp[6]+temp[7],
                           temp[9]+temp[10],
                           temp[11]+temp[12],
                           temp[13]+temp[14]
                          );
        }
        currentEvent[key] = value;
      }

      // create new object if it is the start of a new event
      if(key === "BEGIN" && value === "VEVENT"){
        let index = events.push({});
        currentEvent = events[index-1];
      }

    }
  });
};
